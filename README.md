# HollyJs promo game for Luxoft stand

- [oauth spring boot tutorial](https://spring.io/guides/tutorials/spring-boot-oauth2/)


## REST

- GET state
- POST state/action
- GET state/score
- GET state/error
- POST logout
- GET oauth2/authorization/github

## Production
- [link](http://95.213.191.54/)