# build frontend
FROM node:13 AS NODE_TOOL_CHAIN

COPY holy-frontend/ /tmp/
WORKDIR /tmp/
RUN npm install
RUN npm run build

# build backend
FROM maven:3.6.1-amazoncorretto-11 AS MAVEN_TOOL_CHAIN

COPY holy-backend/pom.xml /tmp/
COPY holy-backend/src /tmp/src/
COPY --from=NODE_TOOL_CHAIN /tmp/dist/ /tmp/src/main/resources/static/

WORKDIR /tmp/

RUN mvn clean package

# production
FROM amazoncorretto:11

EXPOSE 8080

RUN mkdir /app
COPY --from=MAVEN_TOOL_CHAIN /tmp/target/*.jar /app/app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]
