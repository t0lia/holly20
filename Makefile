OS=linux
ARCH=amd64

build:
	docker build -t t0lia/holy20 .

deploy:
	ssh root@95.213.191.54 'docker-compose down && docker-compose pull && docker-compose up -d'

run:
	docker run -d -p 8080:8080 t0lia/holy20

push:
	docker push t0lia/holy20

deliver:
	scp docker-compose.yml root@95.213.191.54:~
