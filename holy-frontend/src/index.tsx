import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './assets/css/style.css'

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import Content from "./components/Content";

const theme = createMuiTheme({
    typography: {
        fontSize: 13,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
});

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <CssBaseline/>
        <Container maxWidth="sm">
            <Content/>
        </Container>
    </ThemeProvider>,
    document.getElementById('main')
);
