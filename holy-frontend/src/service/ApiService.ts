import axios, {AxiosError, AxiosResponse} from 'axios';


export interface GameState {
    score: number;
    game: number[][];
}


class ApiService {
    private static readonly BASE_URL: string = '/';

    constructor() {
        axios.defaults.baseURL = ApiService.BASE_URL;
        axios.interceptors.response.use(ApiService.defaultSuccessHandler, ApiService.defaultErrorHandler);
    }

    private static defaultSuccessHandler(response: AxiosResponse): AxiosResponse {
        return response;
    }

    private static defaultErrorHandler(response: AxiosError): Promise<AxiosError> {
        const code = response.response.status;
        if (code == 401) {
            console.error(`BAD RESPONSE ${response.response}`);
        }
        return Promise.reject(response);
    }


    /**
     * Retrieve filter meta information from server
     */
    public getGameState(success: (response: GameState) => void): void {
        axios.get<GameState>('/state')
            .then(response => response.data)
            .then(success);
    }

    /**
     * Retrieve filter meta information from server
     */
    public updateGameState(success: (response: GameState) => void, action: string): void {
        axios.post<GameState>('/state/' + action)
            .then(response => response.data)
            .then(success);
    }

    public getUser(success: (response: any) => void) {
        axios.get<string>('/state/user')
            .then(response => response.data)
            .then(success);
    }
}

export default new ApiService();