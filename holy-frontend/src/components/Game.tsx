import * as React from 'react';
import {Grid, Paper, Theme} from '@material-ui/core';
import ApiService, {GameState} from "../service/ApiService";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button/Button";
import Typography from "@material-ui/core/Typography/Typography";

interface Props {
    user: string;
}

interface State {
    session: GameState;
}

const styles = ((theme: Theme) => ({
    [theme.breakpoints.up('xs')]: {
        root: {
            '& > *': {
                marginTop: theme.spacing(3),
            },
        },
        paper: {
            height: 135,
            width: 135,
            fontSize: 36,
            margin: 2

        },
        table: {
            width: 480,
            fontSize: 36,
        },
        btn: {marginTop: 10, width: 160, height: 40}
    }
}));

class Game extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            session: {score: 0, game: [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]}
        };
        ApiService.getGameState(state => {
            this.setState({session: state});
        });

    }

    componentDidMount() {
        document.addEventListener("keydown", this.checkKey, false);
    }

    private readonly checkKey = (e: any) => {

        if (e.keyCode == '38') {
            this.handleChange("UP");
            e.preventDefault();
        }
        else if (e.keyCode == '40') {
            this.handleChange("DOWN");
            e.preventDefault();
        }
        else if (e.keyCode == '37') {
            this.handleChange("LEFT");
            e.preventDefault();
        }
        else if (e.keyCode == '39') {
            this.handleChange("RIGHT");
            e.preventDefault();
        }

    };


    private readonly handleChange = (action: string) => {
        ApiService.updateGameState(state => {
            this.setState({session: state});
        }, action)
    };

    private readonly failed = () => {
        return !this.state.session.game.some(line => line.some(value => value === 0));
    };

    private readonly getColor = (number: number) => {
        switch (number) {
            case 0:
                return "#cccccc";
            case 2:
                return "#999999";
            case 4:
                return "#aa9999";
            case 8:
                return "#bb9999";
            case 16:
                return "#cc9999";
            case 32:
                return "#dd9999";
            case 64:
                return "#ee9999";
            case 128:
                return "#ff9999";
            case 256:
                return "#ff8888";
            case 512:
                return "#ff7777";
            case 1024:
                return "#ff6666";
            case 2048:
                return "#ff5555";
            default:
                return "#ff4444";
        }
    };


    render() {

        // @ts-ignore
        const {classes} = this.props;
        return <React.Fragment>
            <Grid item container
                  direction="column"
                  spacing={4}
                  justify="center"
                  alignItems="center"
                  style={{marginTop: 2}}
            >


                <Paper onKeyPress={() => alert("blah")} style={{margin: 15}}>

                    <React.Fragment>

                        <Grid item container
                              direction="column"
                              spacing={4}
                              justify="center"
                              alignItems="center"
                        >


                            <Grid container item justify="center" xs={12}>

                                {this.state.session.game.map(
                                    (line, i) =>
                                        <Grid key={i} container item justify="center" xs={12}>
                                            {line.map((el, j) =>
                                                <Grid key={j} item container alignItems="center" xs={3}>
                                                    {el == 0 ?
                                                        <Paper className={classes.paper}/> :
                                                        <Paper
                                                            className={classes.paper}
                                                            style={{
                                                                backgroundColor: this.getColor(el)
                                                            }}>{el}</Paper>}
                                                </Grid>
                                            )}
                                        </Grid>)}

                                <Grid container item justify="center">

                                    <Grid item xs={4}>
                                        <Button variant="contained" color="primary"
                                                style={{margin: 10}}
                                                onClick={() => {
                                                    this.handleChange("RESET");
                                                }}>НАЧАТЬ ЗАНОВО
                                        </Button>

                                    </Grid>
                                    <Grid item xs={8}>

                                        <Typography color="textPrimary" component="h6" variant="h6" gutterBottom
                                                    style={{float: "right", marginTop: 10, marginRight: 10}}
                                        >
                                            Счет: {this.state.session.score}
                                        </Typography>

                                        {this.failed() &&
                                        <Typography color="textPrimary" component="h6" variant="h6" gutterBottom
                                                    style={{float: "right", marginTop: 10}}>
                                            Вы проиграли.
                                        </Typography>}
                                    </Grid>
                                    <Grid item xs={4}>
                                    </Grid>
                                    <Grid item xs={4}>

                                        <Button variant="contained" className={classes.btn}
                                                onClick={() => {
                                                    this.handleChange("UP");
                                                }}>↑</Button>
                                    </Grid>
                                    <Grid item xs={4}>

                                    </Grid>
                                    <Grid item xs={4}>

                                        <Button variant="contained" className={classes.btn}
                                                onClick={() => {
                                                    this.handleChange("LEFT")
                                                }}>←</Button>
                                    </Grid>
                                    <Grid item xs={4}>


                                    </Grid>
                                    <Grid item xs={4}>

                                        <Button variant="contained" className={classes.btn}
                                                onClick={() => {
                                                    this.handleChange("RIGHT");
                                                }}>→</Button>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Button variant="contained" className={classes.btn}
                                                onClick={() => {
                                                    this.handleChange("DOWN");
                                                }}>↓</Button>
                                    </Grid>
                                </Grid>

                            </Grid>
                        </Grid>

                    </React.Fragment>
                </Paper>
            </Grid>
        </React.Fragment>;
    }

}

export default withStyles(styles)(Game);
