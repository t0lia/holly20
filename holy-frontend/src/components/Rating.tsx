import * as React from 'react';
import {Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Theme} from '@material-ui/core';
import withStyles from "@material-ui/core/styles/withStyles";

import axios from 'axios';

interface ListProps {
}

interface ListState {
    rows: any;
}

const styles = ((theme: Theme) => ({
    [theme.breakpoints.up('xs')]: {
        root: {
            '& > *': {
                marginTop: theme.spacing(3),
            },
        },
        paper: {
            height: 120,
            width: 120,
            fontSize: 36,
            margin: 2

        },
        table: {
            width: 520,
            fontSize: 36,
        },
    }
}));

class Game extends React.Component<ListProps, ListState> {

    constructor(props: ListProps) {
        super(props);
        this.state = {
            rows: [],
        };
        axios.get<string>('/state/score')
            .then((response: any) => {
                console.log(response.data);
                this.setState({rows: response.data});
            });
    }

    render() {

        // @ts-ignore
        const {classes} = this.props;
        return <React.Fragment>


            <Grid item container
                  direction="column"
                  spacing={4}
                  justify="center"
                  alignItems="center"
                  style={{marginTop: 50}}
            >

                <Paper style={{width: 550, padding: 5}}>

                    <React.Fragment>

                        <Grid item container
                              direction="column"
                              spacing={4}
                              justify="center"
                              alignItems="center"
                        >
                            <Grid container item justify="center" xs={12}>
                                <TableContainer className={classes.table} component={Paper}>
                                    <Table className={classes.table} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="center">Место</TableCell>
                                                <TableCell align="center">Игрок</TableCell>
                                                <TableCell align="center">Очки</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {this.state.rows.sort((x: any, y: any) => y.score - x.score).map((row: any, i: number) => (
                                                <TableRow key={row.user}>
                                                    <TableCell align="center">{i + 1}</TableCell>
                                                    <TableCell align="center" component="th" scope="row">
                                                        {row.user}
                                                    </TableCell>
                                                    <TableCell align="center">{row.score}</TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>
                    </React.Fragment>
                </Paper>
            </Grid>
        </React.Fragment>;
    }

}

export default withStyles(styles)(Game);
