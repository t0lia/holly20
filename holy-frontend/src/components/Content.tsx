import * as React from 'react';
import {AppBar, Button, Link, Paper, Tab, Tabs, Theme, Toolbar, Typography} from '@material-ui/core';
import withStyles from "@material-ui/core/styles/withStyles";
import ExitToAppTwoToneIcon from '@material-ui/icons/ExitToAppRounded';
import IconButton from '@material-ui/core/IconButton';
import Avatar from "@material-ui/core/Avatar/Avatar";

import axios from 'axios';
import Rating from "./Rating";
import {Terms} from "./Terms";
import NeedLogin from "./NeedLogin";
import Game from "./Game";
import ApiService from "../service/ApiService";

interface ListProps {
}

interface ListState {
    activeTab: number;
    user: string;
    agree: boolean;
}

const styles = ((theme: Theme) => ({
    [theme.breakpoints.up('xs')]: {
        root: {},
        paper: {
            height: 120,
            width: 120,
            fontSize: 36,
            margin: 2

        },
        table: {
            width: 480,
            fontSize: 36,
        },
    }
}));

class Content extends React.Component<ListProps, ListState> {

    constructor(props: ListProps) {
        super(props);
        this.state = {
            activeTab: 0,
            agree: false,
            user: null,
        };
        ApiService.getUser((user: any) => {
            this.setState({user: user.name});
        });
    }

    private readonly changeTab = (event: React.ChangeEvent<{}>, activeTab: number) => {
        this.setState({activeTab});
    };

    render() {

        // @ts-ignore
        const {classes} = this.props;
        return <React.Fragment>


            <AppBar color="inherit" position="static">
                <Toolbar>

                    <Typography variant="h6"
                                style={{flex: 1}}
                    >
                        {this.state.user &&
                        <div>
                            <IconButton
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                color="inherit"
                                onClick={evt => {
                                }}
                            >

                                <Avatar
                                    className={classes.root}
                                    alt="avatar">
                                    {this.state.user.charAt(0).toUpperCase()}

                                </Avatar>
                            </IconButton>


                            {this.state.user}

                        </div>
                        }
                    </Typography>


                    {this.state.user ? <div>
                            выход
                            <IconButton
                                onClick={e => {
                                    return axios
                                        .post<void>('/logout')
                                        .then(() => {
                                            this.setState({user: null});
                                        });
                                }}>
                                <ExitToAppTwoToneIcon className={classes.root}/>
                            </IconButton>
                        </div>
                        :
                        <div>
                        </div>
                    }


                </Toolbar>

            </AppBar>


            <Paper className={classes.root}>
                <Tabs
                    variant="fullWidth"
                    value={this.state.activeTab}
                    onChange={this.changeTab}
                    indicatorColor="primary"
                    textColor="primary"
                >
                    <Tab label="Игра"/>
                    <Tab label="Рейтинг"/>
                    <Tab label="Правила"/>
                </Tabs>
            </Paper>


            {this.state.activeTab == 0 && this.state.user && <Game user={this.state.user}/>}
            {this.state.activeTab == 0 && !this.state.user && <NeedLogin/>}
            {this.state.activeTab == 1 && <Rating/>}
            {this.state.activeTab == 2 && <Terms/>}

        </React.Fragment>;
    }

}

export default withStyles(styles)(Content);
