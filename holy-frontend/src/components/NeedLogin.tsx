import * as React from 'react';
import {Button, ButtonGroup, Card, CardActions, CardContent, Container, Typography} from '@material-ui/core';
import Grid from "@material-ui/core/Grid/Grid";

import Checkbox from "@material-ui/core/Checkbox/Checkbox";

interface Props {
}

interface State {
    agree: boolean;
}

export default class NeedLogin extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            agree: false,
        };

    }

    private readonly handleAgreeChanged = (): void => {
        this.setState({...this.state, agree: !this.state.agree});
    };

    render() {
        return <React.Fragment>
            <Grid item container
                  direction="row"
                  justify="center"
                  alignItems="center"
                  style={{marginTop: 30}}
            >

                <Grid xs={12} item>
                    <Card>
                        <CardContent>


                            <Typography color="textPrimary" component="h5" variant="h5" gutterBottom>
                                Вход
                            </Typography>

                            <br/>
                            <Typography align="center">
                                <Checkbox
                                    value={this.state.agree}
                                    onChange={this.handleAgreeChanged}
                                    color="primary"
                                />
                                согласен с
                                правилами обработки
                                пользовательских данных.
                            </Typography>

                        </CardContent>
                        <hr/>
                        <CardActions>
                            <Typography color="textPrimary" component="h6" variant="h6" gutterBottom>
                                <Typography color="textPrimary" component="h6" variant="h6" gutterBottom>
                                </Typography>

                                <Container maxWidth="xs">
                                    <ButtonGroup fullWidth size="large" color="primary" aria-label="large outlined primary button group">
                                        <Button variant="contained"
                                                disabled={!this.state.agree}
                                                href="/oauth2/authorization/github">
                                            github
                                        </Button>
                                    </ButtonGroup>
                                </Container>

                            </Typography>

                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </React.Fragment>;
    }
}

