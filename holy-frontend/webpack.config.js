const path = require('path');
const webpack = require('webpack');
const pkg = require('./package.json');

const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = (env, args) => {
    return {
        entry: {
            app: "./src/index.tsx",
            vendor: [
                "react",
                "react-dom",
                "axios"
            ]
        },

        output: {
            filename: "[name].bundle.[contenthash].js",
            chunkFilename: "[name].bundle.[contenthash].js",
            path: path.resolve(__dirname, "dist"),
            publicPath: '/'
        },

        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: [/node_modules/, /__tests__/]
                },
                {
                    test: /\.(css)$/,
                    use: [
                        'style-loader',
                        'css-loader'
                    ],
                    exclude: /node_modules/
                }
            ]
        },
        devtool: "source-map",
        resolve: {
            extensions: ['.js', '.jsx', '.png', '.css', '.tsx', '.ts']
        },
        plugins: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                title: "HolyLuxoft",
                filename: "index.html",
                inject: "body",
                appMountId: "main",
                template: "src/assets/html/template.html",
                favicon: "src/assets/images/favicon.ico"
            }),
            new webpack.DefinePlugin({
                '_APP_VERSION_': JSON.stringify(pkg.version)
            })
        ],

        optimization: {
            splitChunks: {
                chunks: "all",
                name: "vendor"
            }
        },

        devServer: {
            inline: true,
            contentBase: "./dist",
            port: 3000,
            historyApiFallback: true,
            proxy: [{
                path: '/state',
                target: 'http://localhost:8080'
            },{
                path: '/oauth2/',
                target: 'http://localhost:8080'
            },{
                path: '/login/',
                target: 'http://localhost:8080'
            },{
                path: '/logout',
                target: 'http://localhost:8080'
            }, {
                path: '/user/',
                target: 'http://localhost:8080'
            }],
        },

    };
};
