package dev.luxoft.holy20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolyBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(HolyBackendApplication.class, args);
    }
}
