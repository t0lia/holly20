package dev.luxoft.holy20.game.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Game {

    private static final int SIZE = 4;

    private long id;
    private String name;
    private String email;
    private int total;
    private int score = 0;
    private int[][] game = new int[SIZE][SIZE];
}
