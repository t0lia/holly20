package dev.luxoft.holy20.game.console;

import dev.luxoft.holy20.game.domain.Session;

import java.util.HashMap;
import java.util.Map;

public class Printer {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK_FONT = "\u001B[30m";

    public static final String ANSI_BLACK = "\u001B[40m";
    public static final String ANSI_RED = "\u001B[41m";
    public static final String ANSI_GREEN = "\u001B[42m";
    public static final String ANSI_YELLOW = "\u001B[43m";
    public static final String ANSI_BLUE = "\u001B[44m";
    public static final String ANSI_PURPLE = "\u001B[45m";
    public static final String ANSI_CYAN = "\u001B[46m";
    public static final String ANSI_WHITE = "\u001B[47m";

    private static Map<Integer, String> colors = new HashMap<>();

    static {
        colors.put(0, ANSI_WHITE);
        colors.put(2, ANSI_YELLOW);
        colors.put(4, ANSI_GREEN);
        colors.put(8, ANSI_BLUE);
        colors.put(16, ANSI_PURPLE);
        colors.put(32, ANSI_CYAN);
        colors.put(64, ANSI_RED);
    }

    public void print(Session session) {
        for (int i = 0; i < session.getGame().length; i++) {
            for (int j = 0; j < session.getGame()[i].length; j++) {
                System.out.printf(colors.getOrDefault(session.getGame()[i][j], ANSI_RED) + ANSI_BLACK_FONT + "%5d " + ANSI_RESET, session.getGame()[i][j]);
            }
            System.out.println();
        }
        System.out.println("Score: " + session.getScore());
    }

}
