package dev.luxoft.holy20.game.service;

import dev.luxoft.holy20.game.domain.Game;
import dev.luxoft.holy20.game.domain.GameDataRepository;
import dev.luxoft.holy20.game.domain.Session;
import dev.luxoft.holy20.game.dto.UserScore;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameServiceImpl implements GameService {

    private static final int SIZE = 4;

    private GameDataRepository gameDataRepository;

    public GameServiceImpl(GameDataRepository gameDataRepository) {
        this.gameDataRepository = gameDataRepository;
    }

    private void step(Session session, String turn) {
        if (turn == null) {
            return;
        }
        switch (turn) {
            case "LEFT": {
                session.moveLeft();
                break;
            }
            case "UP": {
                session.moveUp();
                break;
            }
            case "RIGHT": {
                session.moveRight();
                break;
            }
            case "DOWN": {
                session.moveDown();
                break;
            }
            case "RESET": {
                session.reset();
                break;
            }
        }
        session.putOneOnField();
    }


    @Override
    public Session getState(String name) {
        Game game = getGameData(name);
        return new Session(game.getScore(), game.getGame());
    }

    @Override
    public List<UserScore> getScores() {
        return gameDataRepository.getAll()
                .stream()
                .map(e -> new UserScore(e.getName(), Integer.toString(e.getTotal())))
                .collect(Collectors.toList());
    }

    @Override
    public Session makeStep(String name, String action) {

        Game game = getGameData(name);

        Session state = new Session(game.getScore(), game.getGame());

        step(state, action);

        game.setGame(state.getGame());
        game.setScore(state.getScore());
        gameDataRepository.save(game);

        if (state.getScore() > game.getTotal()) {
            game.setTotal(state.getScore());
        }

        return state;
    }

    private Game getGameData(String name) {
        if (gameDataRepository.findGameData(name) == null) {
            gameDataRepository.save(new Game(0, name, name, 0, 0, new int[SIZE][SIZE]));
        }

        return gameDataRepository.findGameData(name);
    }

}
