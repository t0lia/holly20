package dev.luxoft.holy20.game.domain;

public enum Turn {
    LEFT, RIGHT, UP, DOWN, QUIT;
}
