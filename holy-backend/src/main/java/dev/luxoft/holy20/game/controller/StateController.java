package dev.luxoft.holy20.game.controller;

import dev.luxoft.holy20.game.dto.UserScore;
import dev.luxoft.holy20.game.service.GameService;
import dev.luxoft.holy20.game.domain.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/state")
public class StateController {

    private final GameService gameService;

    private static final Logger LOG = LogManager.getLogger(StateController.class);

    public StateController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/user")
    @ResponseBody
    public Map<String, Object> user(@AuthenticationPrincipal OAuth2User principal) {
        if (principal == null) {
            return null;
        }
        LOG.warn(principal.toString());
        LOG.warn(principal.getName());
        return Collections.singletonMap("name", principal.getAttribute("login"));
    }

    @GetMapping("/error")
    @ResponseBody
    public String error(HttpServletRequest request) {
        String message = (String) request.getSession().getAttribute("error.message");
        request.getSession().removeAttribute("error.message");
        return message;
    }

    @GetMapping
    public ResponseEntity<Session> get(@AuthenticationPrincipal OAuth2User principal) {
        if (principal == null) {
            return ResponseEntity.ok().body(null);
        }
        String name = ((String) principal.getAttributes().get("login"));
        return ResponseEntity.ok().body(gameService.getState(name));
    }


    @PostMapping("/{action}")
    public ResponseEntity<Session> update(@AuthenticationPrincipal OAuth2User principal,
                                          @PathVariable("action") String action) {

        String name = ((String) principal.getAttributes().get("login"));
        return ResponseEntity.ok().body(gameService.makeStep(name, action));
    }


    @GetMapping("/score")
    @ResponseBody
    public List<UserScore> score(@AuthenticationPrincipal OAuth2User principal) {
        return gameService.getScores();
    }

}
