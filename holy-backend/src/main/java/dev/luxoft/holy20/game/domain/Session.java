package dev.luxoft.holy20.game.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

@Getter
@AllArgsConstructor
public class Session {

    private static final Random RANDOM = new Random();
    private static final int SIZE = 4;

    private int score = 0;
    private int[][] game = new int[SIZE][SIZE];

    public Session() {
        putOneOnField();
    }

    public void moveDown() {

        if (getFreeSpace() == 0) return;
        for (int i = 0; i < game.length; i++) {
            Queue<Integer> queue = new LinkedList<>();
            for (int j = game[i].length - 1; j >= 0; j--) {
                if (game[j][i] != 0) {
                    queue.offer(game[j][i]);
                }
                game[j][i] = 0;
            }
            for (int j = game[i].length - 1; !queue.isEmpty(); j--) {
                Integer poll = queue.poll();
                if (queue.isEmpty() || queue.peek() != poll) {
                    game[j][i] = poll;
                } else {
                    game[j][i] = queue.poll() * 2;
                    score += game[j][i];
                }
            }

        }
    }

    public void reset() {
        score = 0;
        game = new int[SIZE][SIZE];
    }

    public void moveRight() {

        if (getFreeSpace() == 0) return;
        for (int i = 0; i < game.length; i++) {
            Queue<Integer> queue = new LinkedList<>();
            for (int j = game[i].length - 1; j >= 0; j--) {
                if (game[i][j] != 0) {
                    queue.offer(game[i][j]);
                }
                game[i][j] = 0;
            }
            for (int j = game[i].length - 1; !queue.isEmpty(); j--) {
                Integer poll = queue.poll();
                if (queue.isEmpty() || queue.peek() != poll) {
                    game[i][j] = poll;
                } else {
                    game[i][j] = queue.poll() * 2;
                    score += game[i][j];
                }
            }

        }
    }

    public int getFreeSpace() {
        int result = 0;
        for (int i = 0; i < game.length; i++) {
            for (int j = 0; j < game[i].length; j++) {
                if (game[i][j] == 0) {
                    result++;
                }
            }
        }
        return result;
    }

    public void moveLeft() {

        if (getFreeSpace() == 0) return;
        for (int i = 0; i < game.length; i++) {
            Queue<Integer> queue = new LinkedList<>();
            for (int j = 0; j < game[i].length; j++) {
                if (game[i][j] != 0) {
                    queue.offer(game[i][j]);
                }
                game[i][j] = 0;
            }
            for (int j = 0; !queue.isEmpty(); j++) {
                Integer poll = queue.poll();
                if (queue.isEmpty() || queue.peek() != poll) {
                    game[i][j] = poll;
                } else {
                    game[i][j] = queue.poll() * 2;
                    score += game[i][j];
                }
            }

        }
    }

    public void moveUp() {
        if (getFreeSpace() == 0) return;
        for (int i = 0; i < game.length; i++) {
            Queue<Integer> queue = new LinkedList<>();
            for (int j = 0; j < game[i].length; j++) {
                if (game[j][i] != 0) {
                    queue.offer(game[j][i]);
                }
                game[j][i] = 0;
            }
            for (int j = 0; !queue.isEmpty(); j++) {
                Integer poll = queue.poll();
                if (queue.isEmpty() || queue.peek() != poll) {
                    game[j][i] = poll;
                } else {
                    game[j][i] = queue.poll() * 2;
                    score += game[j][i];
                }
            }

        }
    }


    public void putOneOnField() {

        int result = getFreeSpace();
        if (result == 0) {
            return;
        }
        int append = RANDOM.nextInt(10) == 0 ? 4 : 2;
        int position = RANDOM.nextInt(result);
        for (int i = 0; i < game.length; i++) {
            for (int j = 0; j < game[i].length; j++) {
                if (game[i][j] == 0) {
                    if (position == 0) {
                        game[i][j] = append;
                        return;
                    }
                    position--;
                }
            }
        }
    }

}
