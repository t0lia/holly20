package dev.luxoft.holy20.game.service;

import dev.luxoft.holy20.game.dto.UserScore;
import dev.luxoft.holy20.game.domain.Session;

import java.util.List;

public interface GameService {

    Session getState(String name);

    List<UserScore> getScores();

    Session makeStep(String name, String action);
}
