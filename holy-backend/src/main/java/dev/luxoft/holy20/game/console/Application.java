package dev.luxoft.holybackend.game.console;


import dev.luxoft.holy20.game.console.Printer;
import dev.luxoft.holy20.game.domain.Session;
import dev.luxoft.holy20.game.domain.Turn;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static dev.luxoft.holy20.game.domain.Turn.DOWN;
import static dev.luxoft.holy20.game.domain.Turn.LEFT;
import static dev.luxoft.holy20.game.domain.Turn.QUIT;
import static dev.luxoft.holy20.game.domain.Turn.RIGHT;
import static dev.luxoft.holy20.game.domain.Turn.UP;


public class Application {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Printer printer = new Printer();

        Session session = new Session();

        while (true) {
            printer.print(session);
            step(session, waitForCommand(scanner));
        }
    }

    private static Turn waitForCommand(Scanner scanner) {
        return BY_LETTER.get(scanner.nextLine());
    }


    private static void step(Session session, Turn turn) {
        if (turn == null) {
            return;
        }
        switch (turn) {
            case LEFT: {
                session.moveLeft();
                break;
            }
            case UP: {
                session.moveUp();
                break;
            }
            case RIGHT: {
                session.moveRight();
                break;
            }
            case DOWN: {
                session.moveDown();
                break;
            }
            case QUIT: {
                System.out.println("Bye...");
                System.exit(0);
            }

        }
        if (session.getFreeSpace() == 0) {
            System.out.println("You loose!...");
            System.exit(0);
        }
        session.putOneOnField();
    }


    public static final Map<String, Turn> BY_LETTER = new HashMap<>();

    static {
        BY_LETTER.put("q", QUIT);
        BY_LETTER.put("l", RIGHT);
        BY_LETTER.put("h", LEFT);
        BY_LETTER.put("j", DOWN);
        BY_LETTER.put("k", UP);
    }


}
