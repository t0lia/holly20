package dev.luxoft.holy20.game.domain;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Component
public class GameDataRepository {

    private Map<String, Game> gameDataMap;

    public GameDataRepository() {
        gameDataMap = new ConcurrentHashMap<>();
    }

    public Game findGameData(String name) {
        return gameDataMap.get(name);
    }

    public void save(Game data) {
        gameDataMap.put(data.getName(), data);
    }

    public List<Game> getAll() {
        return gameDataMap.values()
                .stream()
                .sorted(comparing(Game::getTotal))
                .collect(toList());
    }
}
